# ZX!BASIC

This is the ZX!BASIC extended BASIC by Stuart Nicholls
published in Popular Computing Weekly (PCW) in August 1985.

Also included are some demo routines published in September
1985 and a sprite designer published in October 1985.

The pages from the magazine are included under folder PCW.
(source: The Internet Archive)

The extension has been checked (using the demos and sprite
designer to exercise it) and some basic testing of the
sprite designer has been done, but there could be some
errors that have cropped up when copying from a JPEG
scan of a very old magazine. In particular there may be
some undiscovered bugs in the sprite designer due to
the scan resolution and small size of the program listing.
