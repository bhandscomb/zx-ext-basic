#include <stdio.h>
#include <stdlib.h>

// quick and dirty hex dump checker
// 000000000011111111112222222222333
// 012345678901234567890123456789012
// xxxx xx xx xx xx xx xx xx xx = xx

int main (int argc, char *argv[])
{
  char line[200];
  int n,c,h,l;
  fgets (line, 200, stdin);
  while (!feof (stdin))
  {
  c = 0;
  for (n = 0; n < 8; n++)
  {
    h = (int)line[5+n*3] - 48;
    if (h > 10) h -= 7;
    l = (int)line[6+n*3] - 48;
    if (l > 10) l -= 7;
    c += (h << 4) | l;
  }
  c &= 0xff;
  h = (int)line[31] - 48;
  if (h > 10) h -= 7;
  l = (int)line[32] - 48;
  if (l > 10) l -= 7;
  n = (h << 4) | l;
  line[4]='\0';
  printf ("%s ", line);
  if (n == c) puts ("OK"); else printf ("ERROR %x != %X\n", c, n);
  fgets (line, 200, stdin);
  }
  return 0;
}

