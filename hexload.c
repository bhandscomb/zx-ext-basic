#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int mem[65536];

int tapchk = 0;

int hex2 (char *p)
{
  int n, l;
  n = (int)*p++ - 48;
  if (n < 0) return 0;
  if (n > 10) n -= 7; // A to F
  l = (int)*p - 48;
  if (l > 10) l -= 7; // A to F
  return (n << 4) | l;
}

int hex4 (char *p)
{
  int n;
  n = hex2 (p) << 8;
  n += hex2 (p+2);
  return n;
}

void tapbyte (FILE *f, int b)
{
  unsigned char u;
  u = (unsigned char) b;
  fwrite (&u, 1, 1, f);
  tapchk ^= b;
}

void loadhexfile (char *name)
{
  FILE *f;
  char line[200];
  f = fopen (name, "r");
  if (f == NULL) return;
  fgets (line, 200, f);
  while (!feof (f))
  {
    int a;
    a = hex4 (&line[0]);
    for (int n = 0; n < 8; n++)
    {
      int b = hex2 (&line[5+n*3]);
      mem [a+n] = b;
    }
    fgets (line, 200, f);
  }
  fclose (f);
}

int main (int argc, char *argv[])
{
  FILE *f;
  int start, end, len, n;
  char tapname[20], zxname[10];
  for (n = 0; n < 65536; n++) mem[n] = -1;
  for (n = 0; n < 10; n++) zxname[n] = ' ';
  if (argc < 4)
  {
    puts ("hexload start end outfile hexfile...");
    return 0;
  }
  start = hex4 (argv[1]);
  end = hex4 (argv[2]);
  if (end == 0) end = 0x10000; // argh! for udg hex dump
  // our "end" is one byte beyond the last byte which is a problem
  // if we need to hex load up to and including memory FFFF!!!
  len = end - start;
  sprintf (tapname, "%s.tap", argv[3]);
  strncpy (zxname, argv[3], strlen(argv[3]));
  while (argc > 4)
  {
    loadhexfile (argv[4]);
    argc--;
    argv++;
  }
  for (n = start; n < end; n += 8)
  {
    printf ("%04X ", n);
    for (int a = 0; a < 8 ; a++)
    {
      if (mem[n+a] < 0) printf ("-- ");
      else printf ("%02X ", mem[n+a]);
    }
    putchar ('\n');
  }
  f = fopen (tapname, "wb");
  if (f == NULL)
  {
    puts ("Unable to write tap");
    return 0;
  }
  // start of block - header
  tapbyte (f, 19); tapbyte (f, 0);
  tapchk = 0;
  tapbyte (f, 0); tapbyte (f,3);
  for (n = 0; n < 10; n++) tapbyte (f, (int) zxname[n]);
  tapbyte (f, len & 0xff); tapbyte (f, len >> 8);
  tapbyte (f, start & 0xff); tapbyte (f, start >> 8);
  tapbyte (f, 0); tapbyte (f, 128);
  tapbyte (f, tapchk);
  // start of block - bytes
  len += 2;
  tapbyte (f, len & 0xff); tapbyte (f, len >> 8);
  tapchk = 0;
  tapbyte (f, 0xff);
  for (n = start; n < end; n++) tapbyte (f, mem[n]);
  tapbyte (f, tapchk);
  fclose (f);
  return 0;
}

